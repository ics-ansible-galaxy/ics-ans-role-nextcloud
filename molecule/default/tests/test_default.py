import os
import testinfra.utils.ansible_runner
import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize("name", [
    "nextcloud_app",
    "nextcloud_postgres",
    "nextcloud_redis",
    "traefik_proxy"
])
def test_nextcloud_containers(host, name):
    with host.sudo():
        container = host.docker(name)
        assert container.is_running


def test_nextcloud_index(host):
    # This tests that traefik forwards traffic to the nextcloud web server
    # and that we can access the nextcloud login page
    cmd = host.command('curl -f -H Host:' + host.ansible.get_variables()["inventory_hostname"] + ' -k -L https://localhost')
    assert cmd.rc == 0 or cmd.rc == 22
    assert '<a href="https://nextcloud.com"' in cmd.stdout or '400 Bad Request' in cmd.stderr or 'The requested URL returned error: 400' in cmd.stderr


def test_nextcloud_script_exist(host):
    assert host.file("/usr/local/bin/elkarbackup-pre-job.sh").exists
