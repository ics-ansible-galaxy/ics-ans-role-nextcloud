ics-ans-role-nextcloud
======================

Ansible role to install [Nextcloud](https://nextcloud.com).

Requirements
------------

- ansible >= 2.7
- molecule >= 2.19

Role Variables
--------------

```yaml
nextcloud_home: /opt/nextcloud
nextcloud_network: nextcloud-network
nextcloud_image: registry.esss.lu.se/ics-docker/nextcloud
nextcloud_tag: 16.0.6
nextcloud_hostname: "{{ ansible_fqdn }}"
nextcloud_redis_tag: 5.0
nextcloud_postgres_tag: 10
nextcloud_postgres_user: postgres
nextcloud_postgres_password: secret
nextcloud_postgres_db: nextcloud
nextcloud_postgres_data: "{{ nextcloud_home }}/pgdata"
nextcloud_html: "{{ nextcloud_home }}/html"
nextcloud_admin_user: admin
nextcloud_admin_password: admin

```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-nextcloud
```

License
-------

BSD 2-clause
